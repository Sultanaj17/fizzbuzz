﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fizzbuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = 0; i < 100; ++i)
            {
                if (i != 0 && i % 3 == 0)
                {
                    Console.Write("FIZZ ");
                }
                else if (i != 0 && i % 5 == 0)
                {
                    Console.Write("BUZZ ");
                }
                else
                {
                    Console.Write($"{i} ");
                }
            }
        }
    }
}
